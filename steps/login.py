import time
from selenium.webdriver.common.keys import Keys


def login(driver,username,password):
    driver.get('https://www.linkedin.com/login')

    inputElement = driver.find_element_by_id("username")
    inputElement.send_keys(username)
    time.sleep(2)

    inputElement = driver.find_element_by_id("password")
    inputElement.send_keys(password)
    time.sleep(2)
    inputElement.send_keys(Keys.ENTER)
    time.sleep(2)

    return driver